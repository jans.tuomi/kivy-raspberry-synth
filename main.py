import kivy
kivy.require('1.10.1') # replace with your current kivy version !

from kivy.config import Config
Config.set('graphics', 'width', '720')
Config.set('graphics', 'height', '480')

from kivy.app import App
from kivy.uix.label import Label


class MyApp(App):

    def build(self):
        return Label(text='Hello world')


if __name__ == '__main__':
    MyApp().run()